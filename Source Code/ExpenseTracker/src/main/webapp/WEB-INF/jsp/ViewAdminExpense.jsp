<%@ include file="common/header.jspf"%>
<%@ include file="common/NavBar.jspf"%>

<div class="container adminLogged">
	<div class="header">
		<!-- <h1>view the account details</h1> -->
		<h1>Your userId is${userId}</h1>
	</div>
	<div>

		<button onclick="exportTableToExcel('tbldata', '${userId}'+'${time}')">Export
			Table Data To Excel File</button>



	</div>
	<div>
		<table class="table table-striped" id="tbldata">
			<caption>Your Expenses</caption>
			<thead>
				<tr>
					<th>Expense</th>
					<th>USER ID</th>
					<th>Type</th>
					<th>product</th>
					<th>Price</th>
					<th>Date</th>


				</tr>
			</thead>
			<c:forEach items="${expense}" var="expense">
				<tr>
					<td>${expense.expenseId}</td>
					<td>${expense.userId}</td>
					<td>${expense.pType}</td>
					<td>${expense.product}</td>
					<td class="price">${expense.price}</td>
					<td>${expense.date}</td>

				</tr>
			</c:forEach>
		</table>
	</div>

	<div>
		<a class="btn btn-primary active btn-success" href="backToAdminHome">Back</a>
	</div>
</div>

<script src="webjars/jquery/1.9.1/jquery.min.js"></script>
<script src="webjars/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script>
     function exportTableToExcel(tableID, filename = ''){
    var downloadLink;
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(tableID);
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
    
    // Specify file name
    filename = filename?filename+'.xls':'excel_data.xls';
    
    // Create download link element
    downloadLink = document.createElement("a");
    
    document.body.appendChild(downloadLink);
    
    if(navigator.msSaveOrOpenBlob){
        var blob = new Blob(['\ufeff', tableHTML], {
            type: dataType
        });
        navigator.msSaveOrOpenBlob( blob, filename);
        System.out.println("inside first scrpt");
    }else{
        // Create a link to the file
        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
    
        // Setting the file name
        downloadLink.download = filename;
        
        //triggering the function
        downloadLink.click();
    }
} </script>
<script src="../script/code.js"></script>
<script src="../script/dollor.js"></script>
<%@ include file="common/Footer.jspf"%>
